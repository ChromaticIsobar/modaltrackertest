#include "../utils/CArray.h"
#include "../utils/AudioConversions.h"
#include <iostream>

#include "../SDT/src/SDT/SDTCommon.h"
#include "../SDT/src/SDT/SDTModalTracker.h"
#include "../SDT/src/SDT/SDTResonators.h"
#include "../SDT/src/SDT/SDTInteractors.h"


std::string getFileName(const std::string& s)
{
	char sep = '/';
#ifdef _WIN32
   sep = '\\';
#endif
   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos)
      return(s.substr(i+1, s.length() - i));
   return(s);
}


std::string getFileNameNoExt(const std::string& s)
{
	std::string t = getFileName(s);
	size_t i = t.rfind('.', t.length());
	if (i != std::string::npos)
		return(t.substr(0, i));
	return(t);
}


int main(int argc, char **argv)
{
	// Load file
	std::cout << "Loading file: ";
	AudioFile<double> af;
	if (argc < 2 || !af.load(argv[1]))
	{
		std::cerr << "[ERROR] Please, specify a valid audio file path as a command-line argument" << std::endl;
		exit(1);
	} else
		std::cout << argv[1] << std::endl;
	DArray v = getAudioSamples( af )[0];
	v /= abs(v).max();

	// Parameters
	long nModes = 128;
	long bufferSize = v.size();
	long winSize = 1<<11;
	double olap = (winSize - 256.0)/winSize;

	std::cout << "Analysing:\n  "
		<< nModes << " modes\n  "
		<< bufferSize << " samples (" << bufferSize/af.getSampleRate() << " seconds) of audio\n  "
		<< winSize << " samples of window size\n  "
		<< olap << " overlap"
		<< std::endl;


	// Build Modal Tracker
	SDT_setSampleRate(af.getSampleRate());
	SDTModalTracker *mt = SDTModalTracker_new(nModes, bufferSize, winSize);
	SDTModalTracker_setOverlap(mt, olap);

	// Read audio into modal tracker
	double *in = (double *) malloc(sizeof(double)*bufferSize);
	for (unsigned int i = 0; i < bufferSize ; ++i) in[i] = v[i];
	SDTModalTracker_readSamples(mt, in, bufferSize);
	
	// Retrieve analysis
	SDTModalTracker_update(mt);
	double *mags, *freqs, *decays;
	mags = (double *) malloc(sizeof(double)*nModes);
	freqs = (double *) malloc(sizeof(double)*nModes);
	decays = (double *) malloc(sizeof(double)*nModes);
	SDTModalTracker_static(mt, mags, freqs, decays);

	free(in);
	SDTModalTracker_free(mt);

	// --- Resynthesis --------------------------------------------------------
	// Build inertial body
	SDTResonator *hammerimp = SDTResonator_new(1,1);
	SDTResonator_setWeight(hammerimp, 0, 0.01);
	SDTResonator_setFragmentSize(hammerimp, 1);
	SDTResonator_setFrequency(hammerimp, 0, 0);
	SDTResonator_setDecay(hammerimp, 0, 0);
	SDTResonator_setGain(hammerimp, 0, 0, 1);
	SDTResonator_setActiveModes(hammerimp, 1);

	// Build modal body
	SDTResonator *objectimp = SDTResonator_new(nModes, 1);
	for (unsigned int mode = 0; mode < nModes; ++mode)
	{
		SDTResonator_setFrequency(objectimp, mode, freqs[mode]);
		SDTResonator_setDecay(objectimp, mode, decays[mode]);
		SDTResonator_setGain(objectimp, 0, mode, mags[mode]);
		SDTResonator_setWeight(objectimp, mode, 1);
	}
	SDTResonator_setFragmentSize(objectimp, 1);
	SDTResonator_setActiveModes(objectimp, nModes);

	// Build impact
	SDTInteractor *imp = SDTImpact_new();
	SDTImpact_setStiffness(imp, 1e+12);
	SDTImpact_setDissipation(imp, 1e+12);
	SDTImpact_setShape(imp, 1.5);
	SDTInteractor_setFirstPoint(imp, 0);
	SDTInteractor_setSecondPoint(imp, 0);
	SDTInteractor_setFirstResonator(imp, hammerimp);
	SDTInteractor_setSecondResonator(imp, objectimp);

	// Do impact
	DArray out(bufferSize);
	for (unsigned int i=0; i < bufferSize; ++i)
	{
		static double tmp[2];
		SDTInteractor_dsp(imp, 0, (i)? 0 : -1, 0, 0, 0, 0, tmp);
		out[i] = tmp[1];
	}

	SDTResonator_free(objectimp);
	SDTResonator_free(hammerimp);
	SDTImpact_free(imp);
	// ------------------------------------------------------------------------

	// Save
	out /= abs(out).max();
	std::string dir = (argc < 3)? "./" : argv[2];
	std::string f_sdtm = dir + "/" + getFileNameNoExt(argv[1]) + "_sdtmodaltracker.wav";
	std::string f_orig = dir + "/" + getFileNameNoExt(argv[1]) + "_original.wav";
	std::cout << "Saving to files:\n  " << f_sdtm << "\n  " << f_orig << std::endl;
	buildAudioFile(out, af.getSampleRate(), 24).save(f_sdtm);
	buildAudioFile(v, af.getSampleRate(), 24).save(f_orig);
}
