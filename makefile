GCC=g++

buildir=build

utils_lib= $(buildir)/utils.a
utils_dir=utils
utils_src= $(wildcard $(utils_dir)/*.cpp)
utils_o  = $(utils_src:.cpp=.o)
utils_obj= $(utils_o:%=$(buildir)/%)

ex_dir=ex
ex_src = $(wildcard $(ex_dir)/*.cpp)
ex_o   = $(ex_src:.cpp=)
ex_obj = $(ex_o:%=$(buildir)/%)

sdt_lib=$(buildir)/SDT.a
sdt_dir =SDT/src/SDT
sdt_src = $(wildcard $(sdt_dir)/*.c)
sdt_o   = $(sdt_src:.c=.o)
sdt_obj = $(sdt_o:%=$(buildir)/%)

af_lib=$(buildir)/AudioFile.a
af_dir =AudioFile
af_src = $(wildcard $(af_dir)/*.cpp)
af_o   = $(af_src:.cpp=.o)
af_obj = $(af_o:%=$(buildir)/%)

OFLAGS=-c
CFLAGS=-std=c++17 -fpermissive -Wnarrowing -O2 -L/usr/X11R6/lib -lm -lpthread -lX11
COMPILE_LIB=rm -f $@ && ar -cvq $@ $^
COMPILE_OBJ=$(GCC) $(OFLAGS) -o $@ $^ $(CFLAGS)
COMPILE_EXE=$(GCC) -o $@ $^ $(CFLAGS)
MKDIR=mkdir -p $(@D)

targets: examples

examples: $(ex_obj)

$(af_lib): $(af_obj)
	$(COMPILE_LIB)

$(sdt_lib): $(sdt_obj)
	$(COMPILE_LIB)

$(utils_lib): $(utils_obj)
	$(COMPILE_LIB)

$(buildir)/$(sdt_dir)/%.o: $(sdt_dir)/%.c
	$(MKDIR)
	$(COMPILE_OBJ)

$(buildir)/$(af_dir)/%.o: $(af_dir)/%.cpp
	$(MKDIR)
	$(COMPILE_OBJ)

$(buildir)/$(utils_dir)/%.o: $(utils_dir)/%.cpp
	$(MKDIR)
	$(COMPILE_OBJ)

$(buildir)/$(ex_dir)/%: $(ex_dir)/%.cpp $(af_lib) $(utils_lib) $(sdt_lib)
	$(MKDIR)
	$(COMPILE_EXE)

.PHONY: clean clear
clean:
	rm -rf $(buildir)/$(sdt_dir) $(buildir)/$(af_dir) $(buildir)/$(utils_dir)
clear:
	rm -rf $(buildir)
