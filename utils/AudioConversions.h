#ifndef AUDIOCONVERSIONS_H
#define AUDIOCONVERSIONS_H

#include <vector>
#include <valarray>
#include "../AudioFile/AudioFile.h"

// --- vector and valarray conversions ----------------------------------------
template <typename T>
inline std::vector<T> valarray2vector ( const std::valarray<T>& v )
/*
	Convert a valarray to a vector
*/
{
	std::vector<T> w;
	w.assign(std::begin(v), std::end(v));
	return w;
}

template <typename T>
inline std::valarray<T> vector2valarray ( const std::vector<T>& v )
/*
	Convert a vector to a valarray
*/
{
	std::valarray<T> w(v.data(),v.size());
	return w;
}

template <typename T>
inline std::vector<std::vector<T>> valarray2vector ( const std::vector<std::valarray<T>>& v )
/*
	Convert a vector of valarrays to a vector of vector
*/
{
	std::vector<std::vector<T>> w;
	for(const std::valarray<T>& u : v)
		w.push_back(valarray2vector(u));
	return w;
}

template <typename T>
inline std::vector<std::valarray<T>> vector2valarray ( const std::vector<std::vector<T>>& v )
/*
	Convert a vector of vectors to a vector of valarrays
*/
{
	std::vector<std::valarray<T>> w;
	for(const std::vector<T>& u : v)
		w.push_back(vector2valarray(u));
	return w;
}
// ----------------------------------------------------------------------------

// --- Build audio file objects -----------------------------------------------
template <typename T>
inline void setSamples (
	AudioFile<T>& af,
	const std::vector<std::vector<T>>& v
)
/*
	Convert a vector of vectors v in audio samples
	 - v.size() is the number of channels
	 - v[i].size() is the number of samples per channel
*/
{
	af.setAudioBufferSize(v.size(),v[0].size());
	for (unsigned int k = 0; k < af.getNumChannels(); k++)
		setSamples(af,v[k],k);
}

template <typename T>
inline void setSamples (
	AudioFile<T>& af,
	const std::vector<T>& v,
	const unsigned int chan = 0
)
/*
	Convert a vector v in audio samples
	 - v.size() is the number of samples
	chan is the channel index - default is 0 (for mono files)
*/
{
	af.setNumSamplesPerChannel(v.size());
	for (unsigned int n = 0; n < af.getNumSamplesPerChannel() ; ++n)
		af.samples[chan][n] = v[n];
}

template <typename T>
inline void setSamples (
	AudioFile<T>& af,
	const std::vector<std::valarray<T>>& v
)
/*
	Convert a vector of valarrays v in audio samples
	 - v.size() is the number of channels
	 - v[i].size() is the number of samples per channel
*/
{
	af.setAudioBufferSize(v.size(),v[0].size());
	for (unsigned int k = 0; k < af.getNumChannels(); k++)
		setSamples(af,v[k],k);
}

template <typename T>
inline void setSamples (
	AudioFile<T>& af,
	const std::valarray<T>& v,
	const unsigned int chan = 0
)
/*
	Convert a valarray v in audio samples
	 - v.size() is the number of samples
	chan is the channel index - default is 0 (for mono files)
*/
{
	af.setNumSamplesPerChannel(v.size());
	for (unsigned int n = 0; n < af.getNumSamplesPerChannel() ; ++n)
		af.samples[chan][n] = v[n];
}

template <typename T, typename U>
inline AudioFile<T> buildAudioFile (
	const std::vector<std::vector<T>>& v,
	const U fs,
	unsigned int bits = 0
)
/*
	Build a multichannel audio file from vector of vectors v
	 - v.size() is the number of channels
	 - the maximum v[i].size() is the number of samples per channel
	fs is the sampling frequency
*/
{
	AudioFile<T> af;

	setSamples(af,v);
	af.setSampleRate(fs);
	if (bits)
		af.setBitDepth(bits);
	else
		af.setBitDepth(8*sizeof(T));

	return af;
}

template <typename T, typename U>
inline AudioFile<T> buildAudioFile (
	const std::vector<T>& v,
	const U fs,
	unsigned int bits = 0
)
/*
	Build a mono audio file from vector v
	 - v.size() is the number of samples
	fs is the sampling frequency
*/
{
	std::vector<std::vector<T>> w;
	w.push_back(v);
	return buildAudioFile(w,fs,bits);
}

template <typename T, typename U>
inline AudioFile<T> buildAudioFile (
	const std::vector<std::valarray<T>>& v,
	const U fs,
	unsigned int bits = 0
)
/*
	Build a multichannel audio file from vector of valarrays v
	 - v.size() is the number of channels
	 - the maximum v[i].size() is the number of samples per channel
	fs is the sampling frequency
*/
{
	AudioFile<T> af;

	setSamples(af,v);
	af.setSampleRate(fs);
	if (bits)
		af.setBitDepth(bits);
	else
		af.setBitDepth(8*sizeof(T));

	return af;
}

template <typename T, typename U>
inline AudioFile<T> buildAudioFile (
	const std::valarray<T>& v,
	const U fs,
	unsigned int bits = 0
)
/*
	Build a mono audio file from valarray v
	 - v.size() is the number of samples
	fs is the sampling frequency
*/
{
	std::vector<std::valarray<T>> w;
	w.push_back(v);
	return buildAudioFile(w,fs,bits);
}
// ----------------------------------------------------------------------------

// --- Read from audio file ---------------------------------------------------
template <typename T>
inline std::vector<std::valarray<T>> getAudioSamples( const AudioFile<T>& af )
/*
	Read audio samples form AudioFile
	Return value is v where
	 - v.size() is the number of channels
	 - v[k].size() is the number of samples per channel
*/
{
	std::vector<std::valarray<T>> v;
	for (unsigned int k = 0 ; k < af.getNumChannels(); ++k)
	{
		std::valarray<T> a(af.getNumSamplesPerChannel());
		for (unsigned int n = 0 ; n < af.getNumSamplesPerChannel() ; ++n)
			a[n] = af.samples[k][n];
		v.push_back(a);
	}
	return v;
}
// ----------------------------------------------------------------------------

// --- Normalization ----------------------------------------------------------
template <typename T>
inline std::valarray<T>& normalize( std::valarray<T>& v )
/*
	Apply gain to the valarray so that the absolute value of the peak is 1
*/
{
	double peak;
	if (peak = std::abs(v).max())
		v *= 1/peak;
	return v;
}
template <typename T>
inline std::valarray<T> getNormalized( const std::valarray<T>& v )
/*
	Apply gain to the valarray so that the absolute value of the peak is 1 (const version)
*/
{
	std::valarray<T> w = v;
	return normalize(w);
}
// ----------------------------------------------------------------------------

#endif
