#ifndef CARRAY_H
#define CARRAY_H

#include <valarray>
#include <vector>
#include <complex>

typedef std::complex<double> Complex;
typedef std::valarray<Complex> CArray;
typedef std::valarray<double> DArray;

// --- Complex to real conversions --------------------------------------------
template <typename T>
inline std::complex<T> real2complex( const T& x ) { return std::complex<T>(x,0); }

template <typename T>
inline T complex2real( const std::complex<T>& x ) { return x.real(); }

template <typename T>
inline T complexl2( const std::complex<T>& x ) { return (x*std::conj(x)).real(); }

template <typename T>
inline std::valarray<std::complex<T>> real2complex( const std::valarray<T>& x )
{
	std::valarray<std::complex<T>> y(x.size());
	for (unsigned int i = 0 ; i < x.size() ; ++i)
		y[i] = real2complex(x[i]);
	return y;
}

template <typename T>
inline std::valarray<T> complex2real( const std::valarray<std::complex<T>>& x )
{
	std::valarray<T> y(x.size());
	for (unsigned int i = 0 ; i < x.size() ; ++i)
		y[i] = complex2real(x[i]);
	return y;
}

template <typename T>
inline std::valarray<T> complexl2( const std::valarray<std::complex<T>>& x )
{
	std::valarray<T> y(x.size());
	for (unsigned int i = 0 ; i < x.size() ; ++i)
		y[i] = complexl2(x[i]);
	return y;
}
// ----------------------------------------------------------------------------

// --- Length conversions -----------------------------------------------------
template <typename T, typename U>
inline std::valarray<T> resize(
	const std::valarray<T>& v,
	const U& len
)
/*
	Output a resized version of v of length len
*/
{
	std::valarray<T> u(len);
	for (unsigned int i = 0 ; (i < v.size()) && (i < u.size()) ; ++i)
		u[i] = v[i];
	return u;
}

template <typename T, typename U>
inline std::vector<std::valarray<T>> resize(
	const std::vector<std::valarray<T>>& v,
	const U& len
)
/*
	Output a resized version of length len of all the valarrays in v
*/
{
	std::vector<std::valarray<T>> u;
	for (const auto& w : v)
		u.push_back(resize(w,len));
	return u;
}
// ----------------------------------------------------------------------------

#endif
