#include "CArray.h"
#include <cmath>

DArray hamming(unsigned int len)
/*
	Produce a hamming window of length len
*/
{
	DArray w(len);
	for (unsigned int n = 0 ; n < len ; ++n)
		w[n] = 0.54 - 0.46*cos(2*(M_PI)*n/(len-1));
	return w;
}

DArray hanning(unsigned int len)
/*
	Produce a hanning window of length len
*/
{
	DArray w(len);
	for (unsigned int n = 0 ; n < len ; ++n)
		w[n] = 0.5 * ( 1 - cos(2*(M_PI)*n/(len-1)) );
	return w;
}

DArray blackmanharris(unsigned int len)
/*
	Produce a Blackman–Harris window of length len
*/
{
	DArray w(len);
	for (unsigned int n = 0 ; n < len ; ++n)
		w[n] = 0.35875 - 0.48829*cos(2*(M_PI)*n/(len-1)) + 0.14128*cos(4*(M_PI)*n/(len-1)) - 0.01168*cos(6*(M_PI)*n/(len-1));
	return w;
}
