#ifndef WINDOWING_H
#define WINDOWING_H

#include "CArray.h"
#include <cmath>

// Produce a hamming window of length len
extern DArray hamming(unsigned int len);

// Produce a hanning window of length len
extern DArray hanning(unsigned int len);

// Produce a Blackman–Harris window of length len
extern DArray blackmanharris(unsigned int len);

#endif
